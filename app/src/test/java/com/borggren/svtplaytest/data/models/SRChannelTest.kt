package com.borggren.svtplaytest.data.models

import org.junit.Test

import org.junit.Assert.*

class SRChannelTest {

    @Test
    fun contentsSameShouldReturnTrueIfAllEquals() {
        val a = SRChannel().apply {
            id = 1
            name = "foo"
            image = "bar"
            tagLine = "lorem"
            color = "#fff"
        }
        val b = SRChannel().apply {
            id = 1
            name = "foo"
            image = "bar"
            tagLine = "lorem"
            color = "#fff"
        }

        assertTrue(a.contentsSame(b))
        assertTrue(b.contentsSame(a))
    }

    @Test
    fun contentsSameShouldReturnFalseIfAllNotEquals() {
        val a = SRChannel().apply {
            id = 1
            name = "foo"
            image = "bar"
            tagLine = "lorem"
            color = "#fff"
        }
        val b = SRChannel().apply {
            id = 2
            name = "bar"
            image = "foo"
            tagLine = "ipsum"
            color = "#000"
        }

        assertFalse(a.contentsSame(b))
        assertFalse(b.contentsSame(a))
    }

    @Test
    fun contentsSameShouldReturnFalseIfNameNotEquals() {
        val a = SRChannel().apply {
            id = 1
            name = "foo"
            image = "bar"
            tagLine = "lorem"
            color = "#fff"
        }
        val b = SRChannel().apply {
            id = 1
            name = "bar"
            image = "bar"
            tagLine = "lorem"
            color = "#fff"
        }

        assertFalse(a.contentsSame(b))
        assertFalse(b.contentsSame(a))
    }

    @Test
    fun contentsSameShouldReturnFalseIfIdNotEquals() {
        val a = SRChannel().apply {
            id = 1
            name = "foo"
            image = "bar"
            tagLine = "lorem"
            color = "#fff"
        }
        val b = SRChannel().apply {
            id = 999
            name = "foo"
            image = "bar"
            tagLine = "lorem"
            color = "#fff"
        }

        assertFalse(a.contentsSame(b))
        assertFalse(b.contentsSame(a))
    }

    @Test
    fun contentsSameShouldReturnFalseIfImageNotEquals() {
        val a = SRChannel().apply {
            id = 1
            name = "foo"
            image = "bar"
            tagLine = "lorem"
            color = "#fff"
        }
        val b = SRChannel().apply {
            id = 1
            name = "foo"
            image = "foo"
            tagLine = "lorem"
            color = "#fff"
        }

        assertFalse(a.contentsSame(b))
        assertFalse(b.contentsSame(a))
    }

    @Test
    fun contentsSameShouldReturnFalseIfColorNotEquals() {
        val a = SRChannel().apply {
            id = 1
            name = "foo"
            image = "bar"
            tagLine = "lorem"
            color = "#fff"
        }
        val b = SRChannel().apply {
            id = 1
            name = "foo"
            image = "bar"
            tagLine = "lorem"
            color = "#000"
        }

        assertFalse(a.contentsSame(b))
        assertFalse(b.contentsSame(a))
    }

    @Test
    fun contentsSameShouldReturnFalseIfTagLineNotEquals() {
        val a = SRChannel().apply {
            id = 1
            name = "foo"
            image = "bar"
            tagLine = "lorem"
            color = "#fff"
        }
        val b = SRChannel().apply {
            id = 1
            name = "foo"
            image = "bar"
            tagLine = "ipsum"
            color = "#fff"
        }

        assertFalse(a.contentsSame(b))
        assertFalse(b.contentsSame(a))
    }
}