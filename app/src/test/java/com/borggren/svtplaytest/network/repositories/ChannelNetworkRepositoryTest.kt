package com.borggren.svtplaytest.network.repositories

import android.accounts.NetworkErrorException
import com.borggren.svtplaytest.data.models.*
import com.borggren.svtplaytest.network.retrofit.SRService
import io.mockk.every
import io.mockk.mockk
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import retrofit2.Call
import retrofit2.mock.Calls

class ChannelNetworkRepositoryTest {

    private lateinit var service: SRService

    @Before
    fun setUp() {
        service = mockk()
    }

    @Test
    fun getChannels() {
        val page = 1
        val size = 10

        val pagination = SRPagination().apply {
            this.page = page
            this.size = size
        }

        val fakeResponse = SRChannelListResponse().apply {
            channels = listOf(SRChannel().apply {
                this.id = 1
            })
        }
        every {
            service.getChannelsWithPagination(
                page = page,
                size = size
            )
        } returns Calls.response(
            fakeResponse
        )

        val repo = ChannelNetworkRepository(service)
        repo.getChannels(pagination) { result, _ ->
            assertEquals(1, result?.channels?.size)
            assertEquals(1, result?.channels?.firstOrNull()?.id)
        }
    }

    @Test
    fun getChannelByIdShouldReturnResultWithChannel() {
        val id = 1
        val fakeResponse = SRChannelDetailResponse().apply {
            channel = SRChannelDetail().apply {
                this.id = 1
            }
        }
        every { service.getChannelById(id) } returns Calls.response(fakeResponse)

        val repo = ChannelNetworkRepository(service)
        repo.getChannelById(id) { result, _ ->
            assertEquals(id, result?.channel?.id)
        }
    }

    @Test
    fun getChannelByIdShouldReturnErrorOnServerFailure() {
        val id = 9999999
        every { service.getChannelById(id) } returns Calls.failure(Throwable("This is an error"))

        val repo = ChannelNetworkRepository(service)
        repo.getChannelById(id) { result, error ->
            assertNull(result)
            assertNotNull(error)
        }
    }
}