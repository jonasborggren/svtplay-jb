package com.borggren.svtplaytest.network.repositories

import com.borggren.svtplaytest.data.models.SRChannelDetailResponse
import com.borggren.svtplaytest.data.models.SRChannelListResponse
import com.borggren.svtplaytest.data.models.SRPagination
import com.borggren.svtplaytest.network.retrofit.SRService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChannelNetworkRepository(private val service: SRService) {

    /**
     * Get channels using [pagination] page and size. The [resultCallback] is called with
     * result optional and error optional and the success check should be where this function
     * is called
     */
    fun getChannels(
        pagination: SRPagination,
        resultCallback: (result: SRChannelListResponse?, error: Throwable?) -> Unit
    ) {
        // Get page and size for pagination
        val page = pagination.page
        val size = pagination.size

        // Create call
        val call = service.getChannelsWithPagination(page = page, size = size)
        call.enqueue(object : Callback<SRChannelListResponse> {
            override fun onFailure(call: Call<SRChannelListResponse>, t: Throwable) {
                resultCallback(null, t)
            }

            override fun onResponse(
                call: Call<SRChannelListResponse>,
                response: Response<SRChannelListResponse>
            ) {
                resultCallback(response.body(), null)
            }
        })
    }

    /**
     * Get channel by [id]. The [resultCallback] is called with result optional and error optional
     * and the success check should be where this function is called
     */
    fun getChannelById(
        id: Int,
        resultCallback: (result: SRChannelDetailResponse?, error: Throwable?) -> Unit
    ) {
        // Create call
        val call = service.getChannelById(id)
        call.enqueue(object : Callback<SRChannelDetailResponse> {
            override fun onFailure(call: Call<SRChannelDetailResponse>, t: Throwable) {
                resultCallback(null, t)
            }

            override fun onResponse(
                call: Call<SRChannelDetailResponse>,
                response: Response<SRChannelDetailResponse>
            ) {
                if (response.isSuccessful) {
                    resultCallback(response.body(), null)
                } else {
                    resultCallback(null, Throwable(response.errorBody()?.string()))
                }
            }

        })
    }

}
