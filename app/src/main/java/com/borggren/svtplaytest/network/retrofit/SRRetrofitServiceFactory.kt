package com.borggren.svtplaytest.network.retrofit

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.simplexml.SimpleXmlConverterFactory

class SRRetrofitServiceFactory {

    /**
     * A function that makes a Retrofit builder and creates the service with the
     * necessary type and returns it
     */
    fun <T> create(classType: Class<T>): T {
        // Create client
        val client = OkHttpClient.Builder().build()

        // The XML factory used is deprecated but since I don't have a huge amount of XML
        // experience I'll keep this since it works in it's current state.
        // I'm sure whoever is using a better way could persuade me easily :)
        val xmlFactory = SimpleXmlConverterFactory.create()

        // Create service
        return Retrofit.Builder()
            .client(client)
            .baseUrl("https://api.sr.se/api/v2/")
            .addConverterFactory(xmlFactory)
            .build()
            .create(classType)
    }

}