package com.borggren.svtplaytest.network

data class Resource<T>(
    var data: T?,
    var error: Throwable?,
    var state: State = State.Idle
) {

    enum class State {
        Loading,
        Completed,
        Failed,
        Idle
    }

    fun isLoading() = state == State.Loading

}