package com.borggren.svtplaytest.network.retrofit

import com.borggren.svtplaytest.data.models.SRChannelDetailResponse
import com.borggren.svtplaytest.data.models.SRChannelListResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface SRService {

    @GET("channels")
    fun getChannelsWithPagination(
        @Query("pagination") pagination: Boolean = true,
        @Query("page") page: Int,
        @Query("size") size: Int
    ): Call<SRChannelListResponse>

    @GET("channels/{id}")
    fun getChannelById(@Path("id") id: Int): Call<SRChannelDetailResponse>

}