package com.borggren.svtplaytest.utils

import android.nfc.tech.MifareUltralight.PAGE_SIZE
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.borggren.svtplaytest.network.Resource


/**
 * Simplifying switching to new [state] inside a viewModel that has [Resource] as wrapper
 */
fun <T> MutableLiveData<Resource<T>>.setState(state: Resource.State) {
    // Post a copy of the existing object with new state
    postValue(this.value?.copy(state = state))
}

/**
 * Use [this] to create a scroll listener calling [nextPage] when reaching the threshold when
 * reaching the bottom. [layoutManager] is used to provide the listener with views visible on
 * screen or anything similar. This will ask the existing adapter and ViewModel using
 * [isLoading] and [isLastPage] to only call [nextPage] when actually necessary.
 *
 * Since this code could be reused multiple times throughout an app's lifecycle this is
 * how we can keep it simple and reusable. Making this an extension-function
 */
fun RecyclerView.addPagination(
    layoutManager: LinearLayoutManager,
    nextPage: () -> Unit,
    isLoading: () -> Boolean,
    isLastPage: () -> Boolean
) {
    addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            if (dy <= 0) return // only continue if scrolling down
            val visibleCount = layoutManager.childCount
            val totalCount = layoutManager.itemCount
            val firstVisibleItemPosition: Int = layoutManager.findFirstVisibleItemPosition()
            val isLastPageReached = isLastPage()
            val isLoadingTrue = isLoading()
            if (!isLoadingTrue && !isLastPageReached) {
                val minimumReached = totalCount >= 10
                val reachedBottom = visibleCount + firstVisibleItemPosition >= totalCount
                val startedScrolling = firstVisibleItemPosition >= 0
                if (reachedBottom && startedScrolling && minimumReached) {
                    nextPage()
                }
            }
        }
    })
}