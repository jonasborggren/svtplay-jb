package com.borggren.svtplaytest.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.borggren.svtplaytest.R
import com.borggren.svtplaytest.data.models.SRChannel
import com.borggren.svtplaytest.data.models.SRChannelListResponse
import com.borggren.svtplaytest.network.Resource
import com.borggren.svtplaytest.network.repositories.ChannelNetworkRepository
import com.borggren.svtplaytest.network.retrofit.SRRetrofitServiceFactory
import com.borggren.svtplaytest.network.retrofit.SRService
import com.borggren.svtplaytest.ui.adapters.SRChannelAdapter
import com.borggren.svtplaytest.ui.viewmodels.ChannelListViewModel
import com.borggren.svtplaytest.utils.addPagination
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), SRChannelAdapter.OnChannelClickCallback {

    private val channelAdapter = SRChannelAdapter(this)
    private val viewModel: ChannelListViewModel by lazy {
        val service = SRRetrofitServiceFactory().create(SRService::class.java)
        ChannelListViewModel(ChannelNetworkRepository(service))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Observe and call for the first page of data
        viewModel.observe(this, channelObserver)
        viewModel.nextChannelPage()

        setupViews()
    }

    /**
     * Observer for data. Since we're using Resource we get a state we can use a switch (when) case
     * for. This will help solidifying the UI
     */
    private val channelObserver = Observer<Resource<SRChannelListResponse>> {
        when (it.state) {
            Resource.State.Loading -> {
                // We're loading, don't hide data since we're using pagination!
                // Show the user that it's loading data
                swipeRefreshLayout.isRefreshing = true
            }
            Resource.State.Completed -> {
                // Success! Inject the data into the adapter and remove the loading indicator
                swipeRefreshLayout.isRefreshing = false
                channelAdapter.submitList(it.data?.channels)
            }
            Resource.State.Failed -> {
                // We ended up here in two cases: either there was an error or data was null.
                // This can be improved by being separated into these two states but since
                // for now we can only end up here if there was an error and since we don't have to
                // maintain this later this is kept this way
                swipeRefreshLayout.isRefreshing = false
            }
            Resource.State.Idle -> {
                // No usage of this state
            }
        }
    }

    /**
     * Setup the views in this activity. This is mostly to free up space in the [onCreate] function
     */
    private fun setupViews() {
        // Setup recyclerView and add pagination functionality.
        // The pagination functionality is moved into an extension function
        // for ease of mind and also to be reused somewhere else
        val layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = channelAdapter
        recyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        recyclerView.addPagination(layoutManager, {
            // Request next page
            viewModel.nextChannelPage()
        }, {
            // Check if the data is already loading to make sure
            // we don't trigger more than once
            viewModel.isLoading()
        }, {
            // Check if we're on the last page to not trigger another
            // page to load when we've reached the bottom
            viewModel.isLastPage()
        })

        // Setup swipe to refresh listener
        swipeRefreshLayout.setOnRefreshListener {
            // Clear adapter
            channelAdapter.clear()
            // Set page to 1 which triggers a call for new data
            viewModel.resetChannelPage()
        }
    }

    override fun onChannelClicked(channel: SRChannel) {
        // Start detail activity by getting intent from target activity
        // The id of the channel is required to start the activity
        startActivity(DetailActivity.getIntent(this, channel.id))
    }

}
