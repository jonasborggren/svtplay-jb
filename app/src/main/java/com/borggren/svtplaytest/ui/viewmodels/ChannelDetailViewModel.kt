package com.borggren.svtplaytest.ui.viewmodels

import androidx.lifecycle.*
import com.borggren.svtplaytest.data.models.SRChannelDetailResponse
import com.borggren.svtplaytest.network.Resource
import com.borggren.svtplaytest.network.repositories.ChannelNetworkRepository
import com.borggren.svtplaytest.utils.setState

class ChannelDetailViewModel(private val repo: ChannelNetworkRepository) : ViewModel() {

    /**
     * This is the main data source of this view model
     */
    private val liveData = MutableLiveData<Resource<SRChannelDetailResponse>>()

    init {
        // Set default state to liveData
        liveData.value = Resource(null, null, Resource.State.Idle)
    }

    /**
     * Observe [liveData] using [owner] with the [observer]. This could be improved further
     * to make sure the same observer is not added more than once
     */
    fun observe(owner: LifecycleOwner, observer: Observer<Resource<SRChannelDetailResponse>>) {
        liveData.observe(owner, observer)
    }

    /**
     * Get [SRChannelDetailResponse] by [id] and post it to [liveData]. Will not start a
     * new call if a call is currently being made
     */
    fun loadChannelById(id: Int) {
        // Check if we're currently loading data, since we don't want concurrent calls happening
        if (liveData.value?.isLoading() == true) return

        liveData.setState(Resource.State.Loading)

        repo.getChannelById(id) { result, error ->
            // Check if data was correctly fetched before posting a success
            if (result != null && error == null) {
                // Success! Post success to LiveData
                liveData.postValue(
                    Resource(
                        data = result,
                        error = null,
                        state = Resource.State.Completed
                    )
                )
            } else {
                // Unknown error, post failure to LiveData
                // This might be due to fields not correctly tied to our
                // retrofit configuration
                liveData.postValue(
                    Resource(
                        data = null,
                        error = error,
                        state = Resource.State.Failed
                    )
                )

            }
        }
    }

}