package com.borggren.svtplaytest.ui.activities

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.borggren.svtplaytest.R
import com.borggren.svtplaytest.data.models.SRChannelDetailResponse
import com.borggren.svtplaytest.network.Resource
import com.borggren.svtplaytest.network.repositories.ChannelNetworkRepository
import com.borggren.svtplaytest.network.retrofit.SRRetrofitServiceFactory
import com.borggren.svtplaytest.network.retrofit.SRService
import com.borggren.svtplaytest.ui.viewmodels.ChannelDetailViewModel
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_detail.*


class DetailActivity : AppCompatActivity() {

    private val viewModel: ChannelDetailViewModel by lazy {
        // Create service
        val service = SRRetrofitServiceFactory().create(SRService::class.java)
        ChannelDetailViewModel(ChannelNetworkRepository(service))
    }

    private var mediaPlayer: MediaPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        setupViews()
        handleIntent()
    }

    /**
     * Handle intent to make sure an id was sent with us here. If there was no id the activity
     * should close OR show a proper error message with the ability to recreate the steps here.
     * For now, this is simply going to show a toast with an error message and leave the activity
     */
    private fun handleIntent() {
        // Get Id from bundle
        val id = intent.extras?.getInt("channel_id")
        if (id != null) {
            viewModel.observe(this, channelDetailObserver)
            viewModel.loadChannelById(id)
        } else {
            // User came here without a channel ID, show error and finish activity
            // A more user friendly error message would be useful
            val errorMessage = getString(R.string.error_channel_not_found)
            handleError(Throwable(errorMessage))
        }
    }

    /**
     * Setup views and click listeners
     */
    private fun setupViews() {
        // Set back button click to toolbar
        toolbar.setNavigationOnClickListener {
            finish()
        }

        // Set defaults
        collapsingToolbarLayout.title = ""
        tagLineView.text = ""
        mediaPlayButton.isEnabled = false
        heroImageView.setImageDrawable(null)
    }

    /**
     * Channel detail observer. Since we're using [Resource] we can achieve
     * state which gives us the possibility to get loading, error and completion status
     */
    private val channelDetailObserver = Observer<Resource<SRChannelDetailResponse>> {
        when (it.state) {
            Resource.State.Loading -> {
                // Show loading
                setupMediaPlayer(null)
            }
            Resource.State.Completed -> {
                // Success, set data to views
                setDataToView(it.data)
                setupMediaPlayer(it.data)
            }
            Resource.State.Failed -> {
                // Error or unknown
                handleError(it.error)
            }
            Resource.State.Idle -> {
                // No usage of this state
            }
        }
    }

    /**
     * Set information to views using [data]. If [data] is sent as null it resets
     * the views to its default state, which in this case would probably look like a bug.
     * A proper handler for this scenario is needed for a production version
     */
    private fun setDataToView(data: SRChannelDetailResponse?) {
        // Set color to toolbar scrim
        if (data != null) {
            val color = Color.parseColor("#${data.channel?.color ?: "333333"}")
            collapsingToolbarLayout.setContentScrimColor(color)
        }
        // Load hero image
        Glide.with(this).load(data?.channel?.image).into(heroImageView)
        // Set title
        collapsingToolbarLayout.title = data?.channel?.name ?: ""
        // Set tag line
        tagLineView.text = data?.channel?.tagLine ?: ""
    }

    /**
     * Setup media player using [data] that holds live audio data.
     * If [data] is sent as null it sets the views to disabled.
     *
     * This function is VERY basic and have no UI feedback such as progress on playback or
     * any other nice things that a media player should include. This is using
     * MediaPlayer and no ExoPlayer or any interface between that helps handling
     * playback events so for this test this is simply a play/pause button
     *
     */
    private fun setupMediaPlayer(data: SRChannelDetailResponse?) {
        val url = data?.channel?.liveAudio?.url
        if (url == null) {
            // No URL found or data was null, disable views
            mediaPlayButton.isEnabled = false
        } else {
            // URL was found, enable views to allow users to play audio
            mediaPlayButton.isEnabled = true
            // Setup new media player with all its configurations
            mediaPlayer = MediaPlayer().apply {
                // Set audio stream type. This is deprecated but the very function they
                // are asking us to use is minSdk 21 so for this example app this is simply
                // going to use the deprecated one since it's no code that will be maintained
                // in the future
                setAudioStreamType(AudioManager.STREAM_MUSIC)
                // Set data source to media player by parsing URL to URI
                setDataSource(baseContext, Uri.parse(url))
                // We've got buffering progress available to use so it's used as secondary progress
                // for our progress bar. This could be turned into a SeekBar to allow user seeking
                setOnBufferingUpdateListener { mp, percent ->
                    mediaProgressView.secondaryProgress = percent
                }
                // Prepare media player and allow for playing in the future
                prepare()
            }

            // Add click listeners to play button. The icon will be a pause one during playback and
            // a play icon during paused or stopped state
            mediaPlayButton.setOnClickListener {
                // Check if player is playing audio to make sure the state is correct
                if (mediaPlayer?.isPlaying == true) {
                    // It's playing! Pause the media and set icon to play
                    mediaPlayer?.pause()
                    mediaPlayButton.setImageResource(R.drawable.ic_play_arrow_white_24dp)
                } else {
                    // It's paused or stopped! Start playing and set icon to pause
                    mediaPlayer?.start()
                    mediaPlayButton.setImageResource(R.drawable.ic_pause_white_24dp)
                }
            }
        }
    }

    /**
     * Handle the error by showing the [throwable] message in a toast and showing the user
     * the front door :)
     */
    private fun handleError(throwable: Throwable?) {
        Toast.makeText(this, throwable?.message ?: "Unknown error", Toast.LENGTH_SHORT).show()
        finish()
    }

    /**
     * Don't forget to release the media player when leaving the activity.
     */
    override fun onDestroy() {
        mediaPlayer?.release()
        mediaPlayer = null
        super.onDestroy()
    }

    companion object {
        /**
         * Return an intent to be used for startActivity(Intent) simplifying code
         * on the other end of it. This helps with clarity since you will be looking in this class
         * if you need to change this later on.
         *
         * The [channelId] is required here so that the view can load proper data
         */
        fun getIntent(context: Context, channelId: Int): Intent {
            val intent = Intent(context, DetailActivity::class.java)
            val extras = Bundle()
            extras.putInt("channel_id", channelId)
            intent.putExtras(extras)
            return intent
        }
    }

}