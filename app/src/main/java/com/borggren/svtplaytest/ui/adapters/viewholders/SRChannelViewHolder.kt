package com.borggren.svtplaytest.ui.adapters.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.borggren.svtplaytest.data.models.SRChannel
import com.borggren.svtplaytest.ui.adapters.SRChannelAdapter
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.cell_channel.view.*

class SRChannelViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bind(channel: SRChannel, clickCallback: SRChannelAdapter.OnChannelClickCallback) {
        // Set text
        itemView.titleView.text = channel.name
        itemView.subtitleView.text = channel.tagLine
        // Load image
        Glide.with(itemView.context).load(channel.image).into(itemView.iconView)
        // Set click callback
        itemView.setOnClickListener { clickCallback.onChannelClicked(channel) }
    }

}