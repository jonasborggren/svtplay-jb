package com.borggren.svtplaytest.ui.viewmodels

import androidx.lifecycle.*
import com.borggren.svtplaytest.data.models.SRChannelListResponse
import com.borggren.svtplaytest.data.models.SRPagination
import com.borggren.svtplaytest.network.Resource
import com.borggren.svtplaytest.network.repositories.ChannelNetworkRepository
import com.borggren.svtplaytest.utils.setState

class ChannelListViewModel(
    private val networkRepository: ChannelNetworkRepository
) : ViewModel() {

    /**
     * The [page] is a live data so that the [liveData] can depend on it.
     * This is to make it easier to trigger a new call whenever the [page] is changed.
     */
    private val page = MutableLiveData<Int>()

    /**
     * This is the main data source of this view model
     */
    private val liveData = MediatorLiveData<Resource<SRChannelListResponse>>()

    init {
        // Set default state to liveData
        liveData.value = Resource(null, null, Resource.State.Idle)
    }

    init {
        // Set the connection between the live data
        liveData.addSource(page) {
            // Page was changed, load the new page of data
            loadChannels(it)
        }
    }

    /**
     * This is to reset the page. This will trigger a call for page 1 since
     * the live data's are "connected" using mediator style
     */
    fun resetChannelPage() {
        page.postValue(1)
    }

    /**
     * Call for next page. This could be done using an external method but since we are
     * holding the live data with [page] we might as well just increase the value from here
     */
    fun nextChannelPage() {
        page.postValue((page.value ?: 0) + 1)
    }

    /**
     * Load channels using a [page]number. This also makes sure to not do concurrent calls
     * as well as change the state (or "status" if you will) so that the UI handles loading data
     * as well as showing data. The postValue to [liveData] is made inside [networkRepository].
     * Will not start a new call if a call is already being made
     */
    private fun loadChannels(page: Int) {
        // Check if data is loading, we don't want concurrent calls
        if (liveData.value?.isLoading() == true) return
        // Create new pagination object
        val pagination = SRPagination().apply {
            this.size = 10
            this.page = page
        }

        liveData.setState(Resource.State.Loading)

        networkRepository.getChannels(pagination) { result, error ->
            if (result != null && error == null) {
                // Success! Post success to LiveData
                liveData.postValue(
                    Resource(
                        data = result,
                        error = null,
                        state = Resource.State.Completed
                    )
                )
            } else {
                // Post failure to LiveData.
                // Also set data in resource to null to make sure UI don't handle it
                liveData.postValue(
                    Resource(
                        data = null,
                        error = error,
                        state = Resource.State.Failed
                    )
                )
            }
        }
    }

    /**
     * Observe [liveData] using [owner] with the [observer]. This could be improved further
     * to make sure the same observer is not added more than once
     */
    fun observe(owner: LifecycleOwner, observer: Observer<Resource<SRChannelListResponse>>) {
        liveData.observe(owner, observer)
    }

    /**
     * Check the state of [liveData]. Returns true if liveData.value.state of type [Resource.State]
     * is [Resource.State.Loading]. If [liveData] value is null it returns true because most
     * likely that's why the value is null -- it's loading currently
     */
    fun isLoading(): Boolean {
        return liveData.value?.isLoading() ?: true
    }

    /**
     * Check if [liveData] pagination has reached the last page. The [SRPagination] field inside
     * [SRChannelListResponse] determine how many pages we could fetch. Returns true if pagination
     * max is not reached yet. Returns false if data is null
     */
    fun isLastPage(): Boolean {
        return liveData.value?.data?.pagination?.isLastPage() == true
    }

}