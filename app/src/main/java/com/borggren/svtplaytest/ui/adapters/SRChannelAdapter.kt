package com.borggren.svtplaytest.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.borggren.svtplaytest.R
import com.borggren.svtplaytest.data.ChannelDiffCallback
import com.borggren.svtplaytest.data.models.SRChannel
import com.borggren.svtplaytest.ui.adapters.viewholders.SRChannelViewHolder

class SRChannelAdapter(private val clickCallback: OnChannelClickCallback) :
    RecyclerView.Adapter<SRChannelViewHolder>() {

    /**
     * List of [SRChannel] to be provided to the adapter. This is the source
     * of information. Usually I would use a ListAdapter but since this is using pagination
     * this is a normal Adapter with a normal list inside
     */
    private val channels = mutableListOf<SRChannel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SRChannelViewHolder {
        // Usually I would put this code into the target ViewHolder so that you would call
        // e.g SRChannelViewHolder.create(context) so that you could find all the code related
        // to it within that same class, but for this case this is kept here
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.cell_channel, parent, false)
        return SRChannelViewHolder(view)
    }

    override fun onBindViewHolder(holder: SRChannelViewHolder, position: Int) {
        holder.bind(channels[position], clickCallback)
    }

    override fun getItemViewType(position: Int): Int {
        // Since this only holds 1 item type we don't have to change the value here
        return super.getItemViewType(position)
    }

    override fun getItemCount(): Int = channels.size

    /**
     * Since we have a swipe to refresh we might need a reset of data. This allows us to
     * clear the existing data set so we can allow a full refresh
     */
    fun clear() {
        val count = channels.size
        channels.clear()
        notifyItemRangeRemoved(0, count)
    }

    /**
     * Submit [list] to the adapter list. This will add it to the bottom of the list
     * since we're supporting pagination.
     */
    fun submitList(list: List<SRChannel>?) {
        // Check if list of new items was empty, don't change the list if that's true
        if (list == null) return
        // List was not null, check for changes in the list and calculate difference
        val newChannels = channels + list
        val diffCallback = ChannelDiffCallback(channels, newChannels)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        // We've now recognized the changes, make sure list has the new items
        channels.clear()
        channels.addAll(newChannels)
        // Dispatch updates to adapter
        diffResult.dispatchUpdatesTo(this)
    }

    interface OnChannelClickCallback {
        fun onChannelClicked(channel: SRChannel)
    }
}