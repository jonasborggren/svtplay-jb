package com.borggren.svtplaytest.data

import androidx.recyclerview.widget.DiffUtil
import com.borggren.svtplaytest.data.models.SRChannel

class ChannelDiffCallback(
    private val oldItems: List<SRChannel>,
    private val newItems: List<SRChannel>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldItems.size
    override fun getNewListSize() = newItems.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldItems[oldItemPosition]
        val newItem = newItems[newItemPosition]
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldItems[oldItemPosition]
        val newItem = newItems[newItemPosition]
        return oldItem.contentsSame(newItem)
    }

}