package com.borggren.svtplaytest.data.models

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "liveaudio", strict = false)
class SRLiveAudio {

    @field:Element(name = "url")
    var url: String? = null

}