package com.borggren.svtplaytest.data.models

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "channel", strict = false)
class SRChannel {

    @field:Attribute(name = "id")
    var id = 0

    @field:Attribute(name = "name")
    var name: String? = null

    @field:Element(name = "image")
    var image: String? = null

    @field:Element(name = "color")
    var color: String? = null

    @field:Element(name = "tagline")
    var tagLine: String? = null

    fun contentsSame(newItem: SRChannel): Boolean {
        if (newItem.id != id) return false
        if (newItem.name != name) return false
        if (newItem.color != color) return false
        if (newItem.image != image) return false
        if (newItem.tagLine != tagLine) return false
        return true
    }
}