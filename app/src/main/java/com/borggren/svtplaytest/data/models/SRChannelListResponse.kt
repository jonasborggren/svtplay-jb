package com.borggren.svtplaytest.data.models

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(name = "sr", strict = false)
class SRChannelListResponse {

    @field:ElementList(entry = "channels", type = SRChannel::class)
    var channels: List<SRChannel> = mutableListOf()

    @field:Element(name = "pagination")
    var pagination: SRPagination? = null

}