package com.borggren.svtplaytest.data.models

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(name = "sr", strict = false)
class SRChannelDetailResponse {

    @field:Element(name = "channel")
    var channel: SRChannelDetail? = null

}