package com.borggren.svtplaytest.data.models

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "channel", strict = false)
class SRChannelDetail {

    @field:Attribute(name = "id")
    var id = 0

    @field:Attribute(name = "name")
    var name: String? = null

    @field:Element(name = "image")
    var image: String? = null

    @field:Element(name = "color")
    var color: String? = null

    @field:Element(name = "tagline")
    var tagLine: String? = null

    @field:Element(name = "liveaudio")
    var liveAudio: SRLiveAudio? = null

}