package com.borggren.svtplaytest.data.models

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "pagination", strict = false)
class SRPagination {

    @field:Element(name = "page")
    var page: Int = 0

    @field:Element(name = "size")
    var size: Int = 10

    @field:Element(name = "totalpages")
    var totalPages: Int = 0

    fun isLastPage(): Boolean {
        return page >= totalPages
    }

    /* I consider these fields unusable for this specific app but they are available
    @Element(name = "totalhits") val totalHits: Int,
    @Element(name = "nextpage") val nextPage: Uri
    */
}